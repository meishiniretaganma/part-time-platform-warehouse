import { Fragment, useState ,useRef} from "react";
import Footer from "../Footer";
import Header from "../Header";
import { SearchOutlined } from "@ant-design/icons"
import { Pagination, Modal, Button, Form, Input, Checkbox, Select, Radio } from 'antd';
import { PhoneOutlined, UserOutlined } from "@ant-design/icons"
import "./index.less"
import Submit from "../Submit";
export default function SelectJob() {
    
    return (
        <Fragment>
            <div style={{ overflow: "hidden" }}>
                <Header>
                    <a className="jz-search">
                        <SearchOutlined className="icon" ></SearchOutlined>
                        搜索
                    </a>
                </Header>
                <div className="w crumbs" style={{ marginTop: "80px" }}>
                    <a href="/">首页</a>
                    &gt;
                </div>

                <div className="filterBar w mt10">
                    <div className="chkFilter">
                        <div id="sex" className="fake-select fl">
                        </div>
                        <div id="worktype" className="fake-select fl">
                            <div className="fake-select-bar">
                                身份要求
                                <i></i></div>
                            <div className="fake-select-wrap">
                                <ul className="fake-select-list">
                                    <li><a href="/jobselect">全部</a></li>
                                    <li><a href="/jobselect">学生可做</a></li>
                                    <li><a href="/jobselect">非学生可做</a></li>
                                </ul>
                            </div>
                        </div>
                        <label title="斗米自营"><input type="checkbox" value="" name="" className="chk" url="/jobselect" />斗米自营</label>
                    </div>
                    <div className="sortFilter">
                        <a href="/jobselect" className="cur">推荐排序</a>
                        <a href="/jobselect">最新发布</a>
                        <a href="/jobselect">工资最高</a>
                    </div>
                </div>

                <div className="jzList-con w">
                    <div className="jzList-item clearfix">
                        <div className="jzList-txt">
                            <div className="jzList-txt-t">
                                <h3>
                                    <a target="_blank" href="/jobdetail">
                                        中国八强金融售后半天班收入十七份
                                    </a>
                                </h3>
                                <i className="word-jipin-ico"></i>
                            </div>
                            <ul className="jzList-field clearfix">
                                <li>
                                    <span>工作时间：全职工作
                                    </span></li>
                                <li><span>工作类型：</span>保险经纪人</li>
                                <li><span>工作地点：</span>河东</li>
                                <li><span>招聘人数：</span>10人</li>
                            </ul>
                        </div>

                        <Submit></Submit>

                        <div className="jzList-salary">
                            <span className="money"><em>7k-20k</em>元/月</span>
                            <span>月结</span>
                        </div>
                    </div>
                </div>
                <Pagination defaultCurrent={1} total={50} className="fengye" />
                <Footer></Footer>
            </div>
        </Fragment>
    );
}