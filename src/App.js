import { Fragment, lazy, Suspense } from "react";
import { Redirect, Route ,Switch} from "react-router";
const Admin = lazy(()=>import("./container/Admin/index"))
const CitySelect = lazy(()=>import("./container/CitySelect/index.jsx"));
const JobDetail = lazy(()=>import("./container/JobDetail"));
const SelectJob = lazy(()=>import("./container/JobSelect"));
const Login = lazy(()=>import("./container/login/login"));
function App() {
  return (
    <Fragment>
      <Suspense fallback={<h1>你这网不行啊，再等等快了....</h1>}>
      <Switch>
      <Route path="/admin" component={Admin}></Route>
      <Route path="/cityselect" component={CitySelect}></Route>
      <Route path="/login" component={Login}></Route>
      <Route path="/jobselect" component={SelectJob}></Route>
      <Route path="/jobdetail" component={JobDetail}></Route>
      <Redirect to="/admin"></Redirect>
      </Switch>
      </Suspense>
    </Fragment>
  );
}

export default App;
