import {useState,useRef} from "react"
import { SearchOutlined } from "@ant-design/icons"
import { Pagination, Modal, Button, Form, Input, Checkbox, Select, Radio } from 'antd';
import { PhoneOutlined, UserOutlined } from "@ant-design/icons"

import "./index.less"
const { Option } = Select;
export default function Submit(){
    const [isModalVisible, setIsModalVisible] = useState(false);
    const myForm = useRef();

    const showModal = () => {
        setIsModalVisible(true);
    };


    const handleOk =() => {
        myForm.current.submit();
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    const onFinish = (values) => {
        console.log('Success:', values);
        setIsModalVisible(false)
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
        setIsModalVisible(true);
    };

    const [value, setValue] = useState(1);

    const onChange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };
    return (
        <div className="jzList-btn">
        <Button type="primary" onClick={showModal}>
            报名参加
        </Button>
        <Modal title='报名' okText="完成报名" cancelText="取消" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <div style={{ padding: "30px 20px" }}>
                <div className="tips">
                    <p>温馨提示：正规兼职不会收取费用，若收费请提高警惕！</p>
                </div>
                <Form
                    ref={myForm}
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 16,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <h2 className="title">确认工作要求和地点</h2>
                    <div style={{ marginBottom: 10 }}>
                        <Form.Item label="选择地点" rules={[{ required: true, message: "得选地方" }]} name="dizhi">
                            <Select defaultValue="成林道1号 (天津 河东)" style={{ width: 268, marginLeft: "20px" }} allowClear>
                                <Option value="成林道1号 (天津 河东)">成林道1号 (天津 河东)</Option>
                            </Select>
                        </Form.Item>
                    </div>
                    <h2 className="title">填写资料并验证手机，可更快上岗并确保工资到账</h2>
                    <div style={{ marginBottom: 10 }}>
                        <Form.Item name="username" label="姓名" rules={[
                            { required: "true", message: "孩子，你的名字没写" },
                            { pattern: /^[\u4E00-\u9FA5]{2,4}$/, message: "你确定你叫这个？" }
                        ]}>
                            <Input prefix={<UserOutlined />} placeholder='请输入真实的名字'></Input>
                        </Form.Item>
                        <Form.Item label="手机号" name="phone" rules={[{ required: true, message: "必须输入手机号" },
                        { max: 11, message: "太长了" },
                        { min: 11, message: "短了，和你一样" },
                        { pattern: /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/, message: "输手机号，这是吗" }
                        ]}>
                            <Input placeholder="手机号" prefix={< PhoneOutlined />} />
                        </Form.Item>
                        <Form.Item name="sex" label="选择性别" rules={[
                            {required:true,message:"对不起，本站不为变性人提供服务"}
                        ]}>
                            <Radio.Group onChange={onChange} value={value}>
                                <Radio value={1}>男</Radio>
                                <Radio value={2}>女</Radio>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item label="学历选择" rules={[{ required: true, message: "文盲么？不该吧" }]} name="xueli">
                            <Select defaultValue="1" style={{ width: 268, marginLeft: "20px" }} allowClear>
                                <Option value="1">初中以下</Option>
                                <Option value="2">初中</Option>
                                <Option value="3">高中</Option>
                                <Option value="4">中专</Option>
                                <Option value="5">大专</Option>
                                <Option value="6">本科</Option>
                                <Option value="7">硕士</Option>
                                <Option value="8">博士及以上</Option>
                            </Select>
                        </Form.Item>
                    </div>
                </Form>
            </div>
        </Modal>
    </div>
    );
}