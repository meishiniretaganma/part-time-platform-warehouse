
import { NavLink } from "react-router-dom";
import Footer from "../Footer";
import Header from "../Header";
import "./index.less";
function Admin(props) {

    return (<div>
        <Header color="#3d3d4a">
            <a className="jz-post">我要招聘
            </a>
        </Header>
        {/* 大的背景图 */}
        <div className="content">
        </div>
        {/* banner 搜索 + 职位推荐*/}
        <div className="banner">
            <div className="banner-text">
                <form action="#" method="get">
                    <div className="search-control">
                        <div className="input-group">
                            <input className="input-text" type="text" autoComplete="off" placeholder="请输入您想要搜索的内容. . ." />
                        </div>
                        <button className="btn-group">搜索</button>
                    </div>
                </form>
                <div className="position">
                    <div className="position-text">
                        <h3>职位推荐</h3>
                        <ul className="position-list recommend-post">
                            <li><NavLink to="/jobselect">服务员</NavLink></li>
                            <li><NavLink to="/jobselect">餐饮工</NavLink></li>
                            <li><NavLink to="/jobselect">促销导购</NavLink></li>
                            <li><NavLink to="/jobselect">送餐员</NavLink></li>
                            <li><NavLink to="/jobselect">快速配送</NavLink></li>
                            <li><NavLink to="/jobselect">传单派发</NavLink></li>
                            <li><NavLink to="/jobselect">问卷调查</NavLink></li>
                            <li><NavLink to="/jobselect">话务客服</NavLink></li>
                            <li><NavLink to="/jobselect">文员</NavLink></li>
                            <li><NavLink to="/jobselect">保姆</NavLink></li>
                        </ul>
                    </div>
                    <div className="position-text">
                        <h3>名企推荐</h3>
                        <ul className="position-list recommend-post">
                            <li><NavLink to="/jobselect">饿了么</NavLink></li>
                            <li><NavLink to="/jobselect">海底捞</NavLink></li>
                            <li><NavLink to="/jobselect">优衣库</NavLink></li>
                            <li><NavLink to="/jobselect">必胜客</NavLink></li>
                            <li><NavLink to="/jobselect">星巴克</NavLink></li>
                            <li><NavLink to="/jobselect">屈臣氏</NavLink></li>
                            <li><NavLink to="/jobselect">好慷家政</NavLink></li>
                            <li><NavLink to="/jobselect">新东方</NavLink></li>
                            <li><NavLink to="/jobselect">名创优品</NavLink></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        {/* 斗米一下 马上入职 */}
        <div className="swiper-container">
            <div className="swiper-wrapper">
                <div className="mod-title">
                    <img src="https://cdn.doumistatic.com/163,0146967f56753220.png" alt="斗米" />
                </div>
            </div>
        </div>

        {/* 高薪全职 */}
        <div className="content-body w">
            <div className="column-box position q">
                <div className="mod-head">
                    <div className="mod-head-left">
                        <div className="title">
                            <h2>高薪全职</h2>
                            <h3>企业直聘有保障 </h3>
                        </div>
                    </div>
                    <div className="mod-head-right">
                        <ul className="mod-list">
                            <li className="mod-item">
                                <a href="/jobselect" >服务员</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/songcanyuan/w2/" data-id="top_5" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_songcanyuan" className="item-info skrollable skrollable-after">送餐员</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/canyingong/w2/" data-id="top_5" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_canyingong" className="item-info skrollable skrollable-after">餐饮工</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/daogou/w2/" data-id="top_1" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_daogou" className="item-info skrollable skrollable-after">促销导购</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/xiaoshou/w2/" data-id="top_7" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_xiaoshou" className="item-info skrollable skrollable-after">销售</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/kefu/w2/" data-id="top_2" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_kefu" className="item-info skrollable skrollable-after">话务客服</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/wenyuan/w2/" data-id="top_6" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_wenyuan" className="item-info skrollable skrollable-after">文员</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/shenheluru/w2/" data-id="top_6" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_shenheluru" className="item-info skrollable skrollable-after">审核录入</a>
                            </li>
                        </ul>
                        <div className="mod-btn"><a className="see-more" href="/tj/w2/" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=gxqz_gd">查看更多全职</a>
                        </div>
                    </div>
                </div>
                <ul className="position-list">
                    <li className="position-item">
                        <a target="_blank" title="津湾广场麦当劳招聘" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=qz_tjw_fuwuyuan" className="item-info">
                            <div className="cate">服务员</div>
                            <div className="name">
                                津湾广场麦当劳招聘
                            </div>
                            <ul className="list">
                                <li className="list-item">全职</li>
                                <li className="list-item"> 和平滨江道 </li>
                                <li className="list-item">招聘10人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">2k-3.5k</b>
                                元/月
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                    <li className="position-item">
                        <a target="_blank" title="永辉河西店理货员" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=qz_tjw_daogou" className="item-info">
                            <div className="cate">促销导购</div>
                            <div className="name">
                                永辉河西店理货员
                            </div>
                            <ul className="list">
                                <li className="list-item">全职</li>
                                <li className="list-item"> 河西挂甲寺 </li>
                                <li className="list-item">招聘5人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">3k-3.6k</b>
                                元/月
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                    <li className="position-item">
                        <a target="_blank" title=" 星级咖啡师" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=qz_tjw_fuwuyuan" className="item-info">
                            <div className="cate">服务员</div>
                            <div className="name">
                                星级咖啡师
                            </div>
                            <ul className="list">
                                <li className="list-item">全职</li>
                                <li className="list-item"> 天津 </li>
                                <li className="list-item">招聘10人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">2.5k-3k</b>
                                元/月
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                    <li className="position-item">
                        <a target="_blank" title="天津项目专员" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=qz_tjw_kfjl" className="item-info">
                            <div className="cate">客服经理</div>
                            <div className="name">
                                天津项目专员
                            </div>
                            <ul className="list">
                                <li className="list-item">全职</li>
                                <li className="list-item"> 河西挂甲寺 </li>
                                <li className="list-item">招聘50人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">6k-8k</b>
                                元/月
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>
                                <i className="icon ziying-ico">自营</i>


                            </div>
                        </a>
                    </li>
                </ul>
                <div className="switch-control q">
                    <i className="prev q"></i>
                    <i className="next q"></i>
                </div>
            </div>
            <div className="column-box position">
                <div className="mod-head">
                    <div className="mod-head-left">
                        <div className="title">
                            <h2>靠谱兼职</h2>
                            <h3>极速上岗结算快</h3>
                        </div>
                    </div>
                    <div className="mod-head-right">
                        <ul className="mod-list">
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/chuandan/w1/" data-id="top_1" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_chuandan">传单派发</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/fuwuyuan/w1/" data-id="top_5" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_fuwuyuan">服务员</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/wenjuandiaocha/w1/" data-id="top_1" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_wenjuandiaocha">问卷调查</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/dbfj/w1/" data-id="top_1" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_dbfj">打包分拣</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/chongchang/w1/" data-id="top_1" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_chongchang">充场</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/shenheluru/w1/" data-id="top_1" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_shenheluru">审核录入</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/canyingong/w1/" data-id="top_5" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_canyingong">餐饮工</a>
                            </li>
                            <li className="mod-item">
                                <a href="/jobselect" data="/tj/zhxz/w1/" data-id="top_2" className="item-info skrollable skrollable-after" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_zhxz">展会协助</a>
                            </li>
                        </ul>
                        <div className="mod-btn"><a className="see-more" href="/tj/w1/" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=kpjz_gd">查看更多兼职</a>
                        </div>
                    </div>
                </div>

                <ul className="position-list">
                    <li className="position-item">
                        <a target="_blank" title="服务员" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=jz_tjw_fuwuyuan" className="item-info">
                            <div className="cate">服务员</div>
                            <div className="name">
                                服务员
                            </div>
                            <ul className="list">
                                <li className="list-item">
                                    <div className="cate">长期可做</div>
                                </li>
                                <li className="list-item"> 南开 </li>
                                <li className="list-item">招聘10人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">14</b>
                                元/小时
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                    <li className="position-item">
                        <a target="_blank" title="服务员" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=jz_tjw_fuwuyuan" className="item-info">
                            <div className="cate">服务员</div>
                            <div className="name">
                                服务员
                            </div>
                            <ul className="list">
                                <li className="list-item">
                                    <div className="cate">长期可做</div>
                                </li>
                                <li className="list-item"> 西青 </li>
                                <li className="list-item">招聘10人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">15</b>
                                元/小时
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                    <li className="position-item">
                        <a target="_blank" title="屈臣氏御泥坊化妆品招聘化妆师学徒（天津河北友谊新都市百货）" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=jz_tjw_meirongmeifa" className="item-info">
                            <div className="cate">美容美发</div>
                            <div className="name">
                                屈臣氏御泥坊化妆品招聘化妆师学徒（天津河北友谊新都市百货）
                            </div>
                            <ul className="list">
                                <li className="list-item">
                                    <div className="cate">长期可做</div>
                                </li>
                                <li className="list-item"> 河北 </li>
                                <li className="list-item">招聘30人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">15</b>
                                元/小时
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                    <li className="position-item">
                        <a target="_blank" title="星巴克星级咖啡师" href="/jobdetail" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=jz_tjw_fuwuyuan" className="item-info">
                            <div className="cate">服务员</div>
                            <div className="name">
                                星巴克星级咖啡师
                            </div>
                            <ul className="list">
                                <li className="list-item">
                                    <div className="cate">长期可做</div>
                                </li>
                                <li className="list-item"> 辛庄 </li>
                                <li className="list-item">招聘10人</li>
                            </ul>
                            <div className="money">
                                <b className="money-bold">16.4</b>
                                元/小时
                            </div>
                            <div className="sign-icon">
                                <i className="icon yj-ico">月结</i>


                            </div>
                        </a>
                    </li>
                </ul>
                <div className="switch-control">
                    <i className="prev"></i>
                    <i className="next"></i>
                </div>
            </div>
            <div className="column-box enterprise">
                <div className="mod-head">
                    <div className="mod-head-left">
                        <div className="title">
                            <h2>名企云集</h2>
                            <h3>丰富知名企业招聘直达</h3>
                        </div>
                    </div>
                    <div className="mod-head-right">
                        <div className="mod-btn"><a className="see-more" dmalog="/jianzhi/index@city=tj@atype=click@ca_source=pc@ca_from=mqbutton" href="/jobselect">查看更多名企</a></div>
                    </div>
                </div>
                <ul className="position-list">
                    <li className="position-item">
                        <a href="/jobselect">
                            <img src="//cdn.doumistatic.com/142,0125ac96266a89c6.png" alt="" />
                            <span>饿了么</span>
                        </a>
                    </li>
                    <li className="position-item">
                        <a href="/jobselect">
                            <img src="//cdn.doumistatic.com/142,0125acb13696ee99.png" alt="" />
                            <span>星巴克</span>
                        </a>
                    </li>
                    <li className="position-item">
                        <a href="/jobselect">
                            <img src="//cdn.doumistatic.com/140,0125acab46941c2b.png" alt="" />
                            <span>麦当劳</span>
                        </a>
                    </li>
                    <li className="position-item">
                        <a href="/jobselect">
                            <img src="//cdn.doumistatic.com/143,0125aca144f35bfa.png" alt="" />
                            <span>海底捞</span>
                        </a>
                    </li>
                    <li className="position-item">
                        <a href="/jobselect">
                            <img src="//cdn.doumistatic.com/141,0125acb6f2da9841.png" alt="" />
                            <span>优衣库</span>
                        </a>
                    </li>
                    <li className="position-item">
                        <a href="/jobselect">
                            <img src="//cdn.doumistatic.com/145,0125ac8b269a045c.png" alt="" />
                            <span>必胜客</span>
                        </a>
                    </li>
                    <li className="position-item">
                        <a href="/jobdetail">
                            <img src="//cdn.doumistatic.com/140,0125aca6a606e7ce.png" alt="" />
                            <span>肯德基</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <Footer></Footer>
    </div>);
}

export default Admin;