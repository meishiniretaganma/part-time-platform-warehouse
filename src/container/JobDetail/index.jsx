import { Fragment } from 'react';
import Header from '../Header';
import { SearchOutlined } from "@ant-design/icons"
import "./index.less"
import Footer from '../Footer';
import Submit from '../Submit';

export default function JobDetail() {
    return (
        <Fragment>
            <Header>
                <a className="jz-search">
                    <SearchOutlined className="icon" ></SearchOutlined>
                    搜索
                </a>
            </Header>
            <div className="w crumbs" style={{ marginTop: "80px" }}>
                <a href="/">首页</a>
                &gt;
            </div>

            <div className="main">
                <div className="jz-d-l-t clearfix">
                    <div className="jz-d-title">
                        <div className="clearfix">
                            <h2>
                                地推销售五险一金
                                <i className="word-ziying-ico"></i></h2>
                        </div>
                        <div className="salary">
                            <span className="fl salary-num">
                                <b>5k-10k</b> 元/月
                            </span>
                            <div className="salary-tips">
                                <span>月结</span>
                                <i className="fl l"></i>
                                <span>地推拉访</span>
                                <i className="fl l"></i>
                                <span>100 人</span>
                            </div>
                        </div>
                    </div>
                    <Submit></Submit>
                </div>
            </div>

            <div className="detail">
                <div className="jz-d-l-b">
                    <div className="m-title">
                        <h3>职位详情</h3>
                        <i className="h-bottom"></i>
                    </div>
                    <div className="jz-d-info">
                        <ul className="jz-condition clearfix">
                            <li>性别不限</li>
                        </ul>
                        <div className="jz-d-area position-info-box" style={{ marginTop: 0 }}>
                            <p >
                                1、有快消、o2o销售、社区团购等 to 小b 销售从业经验；<br />
                                2、有本地社区团长资源者优先；<br />
                                3、自驱力强，具有较强的抗压能力;<br />
                                【工作内容】公司主要从事社区电商，销售主要以小区为单位摸排、招聘社区团长，对商品进行分享，最终达到用户购买<br />
                                【年龄要求】年龄20-35，高中及以上学历<br />
                                【工作时间】早9晚7、做六休一<br />
                                【工作地点】天津主城区就近分配 </p>
                        </div>
                        <a className="btn-down description-down" >展开<i className="btn-down-ico"></i></a>
                        <a className="btn-up description-up" >收起<i className="btn-up-ico"></i></a>
                    </div>
                    <div className="m-title mt50">
                        <h3>薪资待遇</h3>
                        <i className="h-bottom"></i>
                    </div>
                    <div className="jz-d-info salary-welfare">
                        <ul className="jz-welfare clearfix">
                            <li className="welfare-ytc active"><i></i>有提成</li>
                            <li className="welfare-wx active"><i></i>五险</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="didian">
                <div className="jz-d-l-b">
                    <div className="m-title">
                        <h3>工作地点</h3>
                        <i className="h-bottom"></i>
                    </div>
                    <div className="jz-d-info">
                        <div className="bg-work pr-80">
                            <div>
                                <div className="jz-d-area">
                                    天津市河西区南北大街1号凯德天津湾二楼
                                    &nbsp;(
                                    天津市                            河西                            )
                                </div>
                            </div>
                            <div id="work-addr-open" style={{ display: "none" }}>
                                <div className="jz-d-area">
                                    天津市河西区南北大街1号凯德天津湾二楼
                                    &nbsp;(
                                    天津市                            河西                            )
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="m-title mt50">
                        <h3>面试地点</h3>
                        <i className="h-bottom"></i>
                    </div>
                    <div className="jz-d-info">
                        <div className="bg-work pr-80">
                            <div>
                                <div className="jz-d-area">
                                    永辉超市(凯德天津湾店)
                                    &nbsp;(
                                    天津市                        河西区                        )
                                </div>
                            </div>
                            <div style={{ display: "none" }}>
                                <div className="jz-d-area">
                                    永辉超市(凯德天津湾店)
                                    &nbsp;(
                                    天津市                        河西区                        )
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="right">
                <a href="/" className="app-download">
                    <img src="https://cdn.doumistatic.com/157,013ba493cfcdb773.jpg" width="280" alt="" />
                </a>
                <div className="cpy-intro">
                    <div className="cpy-intro-pic">
                        <a href="/">
                            <img src="http://cdn.doumistatic.com/98,cf7a7e0cd06393_120-120_6-0.png" /></a>
                    </div>
                    <div className="cpy-name">
                        <a href="/" style={{ color: "#4b4b4b" }}>
                            天津永辉超市有限公司
                        </a>
                        <i className="certificate"></i>
                    </div>
                    <div className="cpy-intro-link">
                        <a href="/">查看该公司全部信息 &gt;</a>
                    </div>
                </div>
                <div className="mt20">
                    <a href="/">
                        <img src="//cdn.doumistatic.com/145,0124e31968aeaf4b.jpg" alt="更专业的快速招聘"/></a>
                </div>
            </div>
            <Footer></Footer>

        </Fragment>
    );
}