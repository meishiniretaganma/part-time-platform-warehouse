import { Fragment } from "react";
import "./index.less"
import {allCity} from "../../api/allCity"

export default function CitySelect(props) {

    const createCity = (list)=>{
        var a = list.map((item,index)=>{
            return <a href="/" key={index+100}>{item}</a>
        })
        return a;
    }

    function createAll(){
        var all = allCity.city.map((prov,index)=>{
            return (
                <Fragment key={index+100000}>
                    <dt key={index+10000}>{prov.title}</dt>
                    <dd key={index+1000}>
                        {createCity(prov.lists)}
                    </dd>
                </Fragment>
            );
        })
        return all;
    }
    return (
        <Fragment>
            <div className="city-list">
                <div className="bn">
                    <a href="/" className="logo" title="斗米"></a>
                </div>
                <div className="hotCity-list">
                    <dl className="clearfix">
                        <dt>
                            <a className="weizhi" href="/">进入天津</a>
                        </dt>
                        <br />
                        <dt>热门城市：</dt>
                        <dd>
                            <a href="/" key="广州">广州</a>
                            <a href="/" key="北京">北京</a>
                            <a href="/" key="上海">上海</a>
                            <a href="/" key="深圳">深圳</a>
                            <a href="/" key="郑州">郑州</a>
                            <a href="/" key="青岛">青岛</a>
                            <a href="/" key="西安">西安</a>
                            <a href="/" key="石家庄">石家庄</a>
                            <a href="/" key="武汉">武汉</a>
                            <a href="/" key="长沙">长沙</a>
                            <a href="/" key="福州">福州</a>
                            <a href="/" key="苏州">苏州</a>
                            <a href="/" key="南京">南京</a>
                            <a href="/" key="成都">成都</a>
                            <a href="/" key="杭州">杭州</a>
                            <a href="/" key="重庆">重庆</a>
                            <a href="/" key="天津">天津</a>
                            <a href="/" key="南昌">南昌</a>
                        </dd>
                    </dl>
                </div>
                <div className="all-city">
                    <dl>
                        {createAll()}
                    </dl>
                </div>
                <div className="footer"></div>
            </div>
        </Fragment>
    )
}