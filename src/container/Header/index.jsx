import { Fragment, useEffect, useRef, useState} from "react";
import {NavLink, withRouter} from "react-router-dom";
import { Modal, Button } from 'antd';
import "./index.less"
import { connect } from "react-redux";
import { loginOut } from "../../redux/actions/login";
const AMap = window.AMap;
function Header (props) {


    const [location, changeLocation] = useState("北京");
    const content = useRef();


    function dingWei(){
        props.history.push({pathname:"/cityselect",state:{location}});
    }

    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
      setIsModalVisible(true);
    };
  
    const handleOk = () => {
      setIsModalVisible(false);
      props.loginOut();
    };
  
    const handleCancel = () => {
      setIsModalVisible(false);
    };

    // 获取定位
    useEffect(() => {

        content.current.style.background = props.color;
        // changeLocation("北京")

        var map = new AMap.Map('mapContainer', {
            resizeEnable: true
        })
        map.plugin('AMap.Geolocation', function () {
            var geolocation = new AMap.Geolocation({
                // 是否使用高精度定位，默认：true
                enableHighAccuracy: true,
                // 设置定位超时时间，默认：无穷大
                timeout: 10000,
                // 定位按钮的停靠位置的偏移量，默认：Pixel(10, 20)
                buttonOffset: new AMap.Pixel(10, 20),
                //  定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
                zoomToAccuracy: true,
                //  定位按钮的排放位置,  RB表示右下
                buttonPosition: 'RB'
            })
            // 获取当前位置信息
            geolocation.getCurrentPosition();
            // 监听获取位置信息成功的回调函数
            AMap.event.addListener(geolocation, 'complete', onComplete);
            // 监听获取位置信息错误的回调函数
            AMap.event.addListener(geolocation, 'error', onError);

            function onComplete(data) {
                // data是具体的定位信息
                var addComp = data.addressComponent;
                changeLocation(addComp.province);
            }

            function onError(error) {
                // 定位出错
                console.log(error)
            }
        })
    }, [])

    return (<Fragment>
        <header ref={content} className="header" >
            <div className="container">
                <a href="/" className="logo"></a>
                <div className="location" onClick={dingWei}>{location}</div>
                {/* 作为导航栏 */}
                <ul className="nav">
                    <li><NavLink to="/">首页</NavLink></li>
                    <li><NavLink to="/jobselect">全职</NavLink></li>
                    <li><NavLink to="/jobselect">兼职</NavLink></li>
                    <li><a href="#">企业服务</a></li>
                    <li><a href="#" className="last">关于我们</a></li>
                </ul>
                {/* 登录选项 */}
                <div className="login-info">
                    {props.username === "注册登录"?
                    <NavLink to="/login" className="login-reg">注册登录</NavLink>
                        :
                    <Fragment>
                        <a className="login-reg" style={{marginRight:"10px"}}>{props.username}</a>
                        <Button type="primary" onClick={showModal}>
                            退出登录
                        </Button>
                        <Modal title="退出登录" visible={isModalVisible} okText="明天来早点" cancelText="再坐会儿" onOk={handleOk} onCancel={handleCancel}>
                            <p>这才刚进来、不再坐会儿?</p>
                        </Modal>
                    </Fragment>
                }
                </div>
                <a href="" className="jz-app">
                    <i className="app-icon"></i>
                    斗米app
                    <div className="code" id="code">
                        <div className="dm qrcode">
                            <p>斗米App</p>
                            <img src="https://sta.doumi.com/src/image/jianzhi/web/c/v3/dm_qrcode.png" alt="" />
                        </div>
                        <div className="sm qrcode">
                            <p>斗米商户版App</p>
                            <img src="https://sta.doumi.com/src/image/jianzhi/web/c/v3/sh_qrcode.png" alt="" />
                        </div>
                    </div>
                </a>
                {props.children}
            </div>
        </header>
    </Fragment>)
}

export default connect(state=>({
    username:state.login
}),{
    loginOut
})(withRouter(Header));