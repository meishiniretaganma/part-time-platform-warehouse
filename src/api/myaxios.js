import axios from "axios";
import NProgress from "nprogress";
import {message} from "antd";
import "nprogress/nprogress.css";

let instance = axios.create({
    timeout:5000,
    baseURL:"/api"
})
instance.interceptors.request.use((config)=>{
    NProgress.start();
})
instance.interceptors.response.use((response)=>{
    NProgress.done();
    return new Promise.resolve(response.data);
},(error)=>{
    NProgress.done();
    message.error(error.message,1);
    return new Promise(()=>{});
})
export default instance;